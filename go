#!/usr/bin/env bash

set -e

if [ ! -x "$(which stack)" ] ; then
  brew install haskell-stack
fi

stack setup --silent

case "$1" in

    build)
        stack build
        stack test
        ;;
    run)
        stack build
        stack exec shokunin-feature-dep-graph-exe
        ;;
    "")
        stack build
        stack test
        stack exec shokunin-feature-dep-graph-exe
        ;;
    profile)
        stack clean
        stack test \
            --executable-profiling \
            --library-profiling \
            --ghc-options="-fprof-auto" \
            --test-arguments "--quickcheck-tests 1000 --quickcheck-max-size 200 +RTS -N -P"
        echo "Done! See .prof file"
        ;;
    *)
        echo "Usage"
        echo "./go build      # runs build & tests"
        echo "./go run        # runs app reading from console (builds first if necessary)"
        echo "./go profile    # builds & runs tests with a lot of iterations & larger inputs (produces .prof file)"
        echo "./go            # builds, tests and runs the app"
        ;;

esac