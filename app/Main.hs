module Main where

import DependencySolver (run)
import Data.List

main :: IO ()
main = do
    _ <- putStrLn "Reading from standard in, Ctrl-D to finish (or Ctrl-Z Enter on Windows) ..."
    input <- getContents
    putStrLn $ either (intercalate "\n") id (run input)
