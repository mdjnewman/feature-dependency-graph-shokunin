
module Main where

import Test.Tasty

import DependencySolverSpec
import ParsingSpec
import GraphSpec
import ValidationSpec

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "All tests"
    [ parsingTests
    , validationTests
    , graphTests
    , dependencySolverTests
    ]
