module DependencySolverSpec (dependencySolverTests) where

import Test.Tasty
import Test.Tasty.HUnit

import DependencySolver

dependencySolverTests :: TestTree
dependencySolverTests = testGroup "Dependency solver tests"
    [ testCase "Example from problem specification" $
        Right "A,B,C,D,E,F\n\
               \G,H\n\
               \I\n\
               \J"
        @=?
        run "(A,B,C,G,H)[G->A,H->A,H->B]\n\
            \(D,E,F,I,J)[I->D,I->E,J->F,J->I,I->H]"

    , testCase "Two missing dependents in groups" $
        Left [ "Dependent feature(s) G are not in group (A,B)[G->A]"
             , "Dependent feature(s) I are not in group (D,E)[I->D]"
             ]
        @=?
        run "(A,B)[G->A]\n\
            \(D,E)[I->D]"

    , testCase "Determinant feature does not exist" $
        Left [ "Features(s) D are not in any group" ]
        @=?
        run "(A,B)[A->B,B->A,A->D]"

    , testCase "With cycle" $
        Left [ "There are circular dependencies, aborting" ]
        @=?
        run "(A,B)[A->B,B->A]"

    , testCase "Four feature transitive closure + one free feature" $
        Right "D,E\n\
               \C\n\
               \B\n\
               \A"
        @=?
        run "(A,B,C,D,E)[A->B,A->C,A->D,B->C,B->D,C->D]"

    , testCase "No dependencies" $
        Right "A,B,C,D" @=? run "(A,B,C,D)"

    ]

