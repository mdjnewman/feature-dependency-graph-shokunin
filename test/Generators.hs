module Generators
    ( genComplicatedFeatureName
    , genSimpleFeatureName
    , genGroupWithNoDepedencies
    , genGroup
    , genGraph)
    where

import Test.Tasty.QuickCheck

import Data.Text (pack, unpack, strip)
import Data.List
import qualified Data.Set as Set
import Data.Maybe (fromJust) -- TODO remove this nasty hack

import DependencySolver.Internal.Domain
import DependencySolver.Internal.Graph

genComplicatedFeatureName :: Gen String
genComplicatedFeatureName = do
    name <- suchThat chars (any (`notElem` "\t\n\r\f\v "))
    return $ unpack . strip . pack $ name
    where
        asciiChar = choose ('\32' , '\126')
        anyChar = choose (minBound :: Char, maxBound :: Char)
        chars = listOf1 $ suchThat (frequency [(1, anyChar), (10, asciiChar)]) (`notElem` excludedFeatureNameCharacters)

genSimpleFeatureName :: Gen String
genSimpleFeatureName = vectorOf 3 $ elements $ ['A'..'Z'] ++ ['a'..'z'] ++ ['0'..'9']


genGroupWithNoDepedencies :: Gen String -> Gen Group
genGroupWithNoDepedencies nameGen = do
    feats <- genUniqueFeatures nameGen
    deps <- pure []
    return $ Group (Set.fromList feats) (Set.fromList deps)

genGroup :: Gen String -> Gen Group
genGroup nameGen = do
    feats <- genUniqueFeatures nameGen
    deps <- makeRandomDependenciesBetweenSortedFeatures feats
    return $ Group (Set.fromList feats) (Set.fromList deps)


genGraph :: Gen Group -> Gen FeatureGraph
genGraph g = do
    groups <- listOf1 g
    return $ buildGraph groups

-------------------------------------------------------------------------------

makeRandomDependenciesBetweenSortedFeatures :: [Feature] -> Gen [Dependency]
makeRandomDependenciesBetweenSortedFeatures feats =
    concat <$> mapM (genDependenciesForFeature feats) feats

-- | Given a feature, randomly generate dependencies for 50% of the
-- | features following it in the list
genDependenciesForFeature :: [Feature] -> Feature -> Gen [Dependency]
genDependenciesForFeature fs f = do
    indexes <- chooseIndices ((+ 1) $ fromJust $ elemIndex f fs) (length fs - 1)
    return $ fmap (\i -> f `dependsOn` (fs !! i)) indexes

    where
        chooseIndices :: Int -> Int -> Gen [Int]
        chooseIndices minIndex maxIndex = case [minIndex..maxIndex] of
            x@(_:_) -> vectorOf (max 1 (floor $ fromIntegral (maxIndex - minIndex) / (2 :: Double))) $ elements x
            []      -> pure []


genUniqueFeatures :: Gen String -> Gen [Feature]
genUniqueFeatures nameGen = fmap (nub .fmap Feature) (listOf1 nameGen)


