module ValidationSpec (validationTests) where

import Test.Tasty
import Test.Tasty.HUnit
import qualified Data.Set as Set

import DependencySolver.Internal.Domain
import DependencySolver.Internal.Validation

validateGroups :: [Group] -> Either [ErrorMessage] [Group]
validateGroups gs = validateAllDeterminantFeaturesExist gs >>= validateDependentFeaturesAreIncludedInGroup

happyPathTestInputs :: [(String, [Group])]
happyPathTestInputs =
    [ ("Validate empty list of groups succeeds", [])

    , ("Validate single empty group succeeds", [Group Set.empty Set.empty])

    , ("Validate single group with one feature succeeds", [Group (Set.singleton $ Feature "A") Set.empty])

    , ("Validate single group with two features and one valid dependency succeeds",
        [Group (Set.fromList [Feature "A", Feature "B"]) (Set.singleton (Feature "B" `dependsOn` Feature "A"))])

    , ("Validate two groups where determinant is in another group succeeds",
        [ Group (Set.singleton (Feature "A")) (Set.fromList [Feature "A" `dependsOn` Feature "B"])
        , Group (Set.singleton (Feature "B")) Set.empty
        ])
    ]

generateHappyPathTest :: (String, [Group]) -> TestTree
generateHappyPathTest (msg, input) = testCase msg $ Right input @=? validateGroups input

happyPathTests :: TestTree
happyPathTests = testGroup "Happy validation paths" $ map generateHappyPathTest happyPathTestInputs

sadPathTests :: TestTree
sadPathTests = testGroup "Sad validation paths"
    [ testCase "Validate group with missing dependent fails" $
        Left ["Dependent feature(s) A are not in group (B)[A->B]"]
        @=?
        validateDependentFeaturesAreIncludedInGroup [Group (Set.singleton $ Feature "B") (Set.singleton $ Feature "A" `dependsOn` Feature "B")]

    , testCase "Validate groups where determinant feature does not exist should fail" $
        Left ["Features(s) A are not in any group"]
        @=?
        validateAllDeterminantFeaturesExist [Group (Set.singleton $ Feature "B") (Set.singleton $ Feature "B" `dependsOn` Feature "A")]
    ]

validationTests :: TestTree
validationTests = testGroup "Validation tests" [ happyPathTests, sadPathTests ]

