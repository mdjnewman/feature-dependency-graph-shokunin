module GraphSpec (graphTests) where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Data.Graph.Wrapper
import qualified Data.Set as Set

import Control.Applicative
import Data.List (elemIndex)
import Data.Maybe (fromMaybe)

import Generators

import DependencySolver.Internal.Domain
import DependencySolver.Internal.Graph

graphTests :: TestTree
graphTests = testGroup "Compute execution set tests"
    [ testCase "Sets for simple sequential graph are correct" $
        [[Feature "C"],[Feature "B"],[Feature "A"]]
        @=?
        computeExecutionSets (fromListSimple
                                [ (Feature "A", [Feature "B"])
                                , (Feature "B", [Feature "C"])
                                , (Feature "C", [])
                                ])

    , testProperty "All features in input group should be in output execution set" $
        forAll (genGroup genSimpleFeatureName) allNodesInGroupAreInExecutionSet

    , testProperty "Graphs with no dependencies should always have one execution set" $
        forAll (resize 10 $ genGraph . genGroupWithNoDepedencies $ genSimpleFeatureName) (\x -> length (computeExecutionSets x) == 1)

    , testProperty "All features in group should be after their determinants in execution set" $
        forAll
            (quarterSize $ noCyclesGenerator $ listOf1 $ genGroup genSimpleFeatureName)
            allNodesInGraphAreAfterTheirDeterminantsInExecutionSet
    ]

quarterSize :: Gen a -> Gen a
quarterSize = scale (\i -> ceiling ( fromIntegral i / (4 :: Float) ) )

allNodesInGroupAreInExecutionSet :: Group -> Bool
allNodesInGroupAreInExecutionSet g =
    Set.size (Set.difference inputFeatures outputFeatures) == 0
    where
        inputFeatures = features g
        outputFeatures = Set.fromList $ concat $ computeExecutionSets $ buildGraph [g]


allNodesInGraphAreAfterTheirDeterminantsInExecutionSet :: [Group] -> Bool
allNodesInGraphAreAfterTheirDeterminantsInExecutionSet g = and $
    fmap
        (\x -> fromMaybe False $ liftA2 (>) (dependentIndex x) (determinantIndex x))
        (asTuple <$> Set.elems (allDependencies g))
    where
        dependentIndex x = elemIndex (fst x) solution
        determinantIndex x = elemIndex (snd x) solution
        solution :: [Feature]
        solution = concat $ computeExecutionSets $ buildGraph g

noCyclesGenerator :: Gen [Group] -> Gen [Group]
noCyclesGenerator gen = suchThat gen noCycles
    where
        noCycles gs = case checkForCycles (buildGraph gs) of
            Left _ -> False
            Right _ -> True
