module ParsingSpec (parsingTests) where

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Data.Either (isLeft)
import qualified Data.Set as Set

import Generators

import DependencySolver.Internal.Domain
import DependencySolver.Internal.Parser

parsingTests :: TestTree
parsingTests = testGroup "Parsing tests"
    [ testCase "Empty string contains no groups" $
        Right []
        @=?
        parseGroups ""

    , testCase "Whitespace is not a legitimate feature name" $
        True
        @=?
        isLeft (parseGroups "( \r\n\t    , B )\n")

    , testCase "Single group with no dependency list is parsed" $
        Right [mkGroup [Feature "A", Feature "B"] []]
        @=?
        parseGroups "( A    , B )\n"

     , testCase "Single group with empty dependency list is parsed" $
        Right [mkGroup [Feature "A", Feature "B"] []]
        @=?
        parseGroups "(A,B)[]"

     , testCase "Feature name with space in it is parsed" $
        Right [mkGroup [Feature "aaa bb"] []]
        @=?
        parseGroups "(aaa bb)[]"

    , testCase "Two groups with no separator are parsed" $
        Right [mkGroup [] [], mkGroup [] []]
        @=?
        parseGroups "()()\n\n"

    , testCase "Two groups with multiple newline separators are parsed" $
        Right [mkGroup [] [], mkGroup [] []]
        @=?
        parseGroups "()\n\n\n\r\n\r\r()"

    , testCase "Single group with two dependencies is parsed" $
        Right   [ mkGroup
                    [ Feature "A", Feature "B" ]
                    [ Feature "A" `dependsOn` Feature "B", Feature "B" `dependsOn` Feature "A"]
                ]

        @=?
        parseGroups "(  A , B    )[ A     -> B ,   B->A]\n"

    , testProperty "parseGroups (show x) == Right[x]" $
        forAll (resize 20 $ genGroup genComplicatedFeatureName) $ \x -> parseGroups (show x) == Right [x]

    ]


mkGroup :: [Feature] -> [Dependency] -> Group
mkGroup f d = Group (Set.fromList f) (Set.fromList d)
