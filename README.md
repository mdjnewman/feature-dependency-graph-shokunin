Feature Dependency Graph Solver
===============================

To build & run tests (requires [`stack`](http://www.haskellstack.org/)
installed, or [`brew`](http://brew.sh/) available to install `stack` if it's missing):

```
./go build
```

To just run the app, reading input from the command line:

```
./go run
```

Solution uses a version of the [Coffman–Graham
algorithm](https://en.wikipedia.org/wiki/Coffman%E2%80%93Graham_algorithm).

Origin Problem Statement
------------------------

In this challenge you must calculate the correct order in which to evaluate a
collection of features, given that some features depend on other features.

What's a feature? It's a data science term (see this Wikipedia page). However,
for the purposes of this challenge, all you need to know is that a feature
represents an isolated unit of work that may depend on the output of other
features. Because of this relationship, it's important that features are
evaluated in the correct order.

Features belong to groups and can have dependencies in other groups, like this:

![Dependency graph example](example-depedency-graph.png)

Your mission, should you choose to accept it, is to create a program that
determines the optimal way to allocate features to execution sets. (i.e What is
the order in which we should execute these features?)

The input for the above diagram would look like this:

    (A,B,C,G,H)[G->A,H->A,H->B]
    (D,E,F,I,J)[I->D,I->E,J->F,J->I,I->H]

The `()` part specifies the set of features that exist in that group.

The `[]` part specifies the feature dependencies. They can point to features in
the current group or other groups.

So the output would look like:

    A,B,C,D,E,F
    G,H
    I
    J

Each line would represent a set of things that can be evaluated because either
they have 0 dependencies or all their dependencies have been evaluated from
previous sets. (i.e including transitive dependencies)

Other tips / info:

* If there is a circular dependency it should display an error. (or similar feedback)
* There is no limit to the amount of features that can exist in an execution set.
* Remember that each feature should be independently executable so the order of
  features in an execution result set is not important. But the order of the
  execution sets is important!
* Groups names are not important and can just be assigned an arbitrary name /
  index.
* If a feature dependency is defined ie `[A->B]` and `A` does not exist in the
  current group set, it should error.
* If a feature dependency is defined ie `[A->B]` and `B` does not exist in any group
  set, it should error.