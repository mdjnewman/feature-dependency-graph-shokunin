module DependencySolver (run) where

import Data.List (sort, intercalate)

import DependencySolver.Internal.Domain
import DependencySolver.Internal.Parser (parseGroups)
import DependencySolver.Internal.Validation
import DependencySolver.Internal.Graph (buildGraph, checkForCycles, computeExecutionSets)

run :: String -> Either [ErrorMessage] String
run x = parseGroups x
            >>= validateAllDeterminantFeaturesExist
            >>= validateDependentFeaturesAreIncludedInGroup
            >>= pure . buildGraph
            >>= checkForCycles
            >>= pure . computeExecutionSets
            >>= pure . map sort
            >>= pure . intercalate "\n" . map (intercalate "," . map show)
