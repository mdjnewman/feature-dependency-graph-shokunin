module DependencySolver.Internal.Domain
    ( Feature (..)
    , Dependency(asTuple)
    , dependsOn
    , dependent
    , determinant
    , Group (..)
    , ErrorMessage
    , ExecutionSet
    , excludedFeatureNameCharacters
    , allFeatures
    , allDependencies
    ) where

import Data.List (intercalate)

import Data.Set (Set)
import qualified Data.Set as Set

type ErrorMessage = String

newtype Feature = Feature { getName :: String } deriving (Eq, Ord)

newtype Dependency = Dependency { asTuple :: (Feature,Feature) } deriving (Eq, Ord)

data Group = Group { features :: Set Feature, dependencies :: Set Dependency } deriving (Eq)

type ExecutionSet = [Feature]

instance Show Feature where
    show = getName

instance Show Dependency where
    show (Dependency (a,b))  = show a ++ "->" ++ show b

instance Show Group where
    show g = "(" ++ printFeats ++ ")" ++ "[" ++ printDeps ++ "]"
        where
            printFeats = intercalate "," (show <$> Set.elems (features g))
            printDeps = intercalate "," (show <$> Set.elems (dependencies g))

dependent :: Dependency -> Feature
dependent (Dependency (x,_)) = x

determinant :: Dependency -> Feature
determinant (Dependency (_,x)) = x

dependsOn :: Feature -> Feature -> Dependency
dependsOn a b = Dependency (a,b)

excludedFeatureNameCharacters :: String
excludedFeatureNameCharacters = ",)->]"

allDependencies :: [Group] -> Set Dependency
allDependencies gs = Set.unions $ map dependencies gs

allFeatures :: [Group] -> Set Feature
allFeatures gs = Set.unions $ map features gs
