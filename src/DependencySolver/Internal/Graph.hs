module DependencySolver.Internal.Graph
    ( FeatureGraph
    , buildGraph
    , checkForCycles
    , computeExecutionSets
    , addFeatureToExecutionSets
    , findCompatibleExecutionSet
    ) where

import DependencySolver.Internal.Domain

import           Control.Arrow (second)
import           Data.Graph.Wrapper
import           Data.List.Utils (flipAL)
import           Data.Maybe

import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

import           Data.Set (Set)
import qualified Data.Set as Set

type FeatureGraph = Graph Feature Feature


buildGraph :: [Group] -> FeatureGraph
buildGraph groups = fromVerticesEdges feats deps
    where
        feats =  (\x -> (x,x)) <$> Set.elems (allFeatures groups)
        deps =  asTuple <$> Set.elems (allDependencies groups)


checkForCycles :: FeatureGraph -> Either [ErrorMessage] FeatureGraph
checkForCycles g =
    if hasCycles
    then
        Left ["There are circular dependencies, aborting"]
    else
        Right g
    where
        hasCycles = or $ fmap (\x -> hasPath g (snd x) (fst x)) (edges g)


computeExecutionSets :: FeatureGraph  -> [ExecutionSet]
computeExecutionSets g = fromMap $ foldl (addFeatureToExecutionSets edgesMap) Map.empty (reverse $ topologicalSort g)
    where
        fromMap :: Map Feature Int -> [ExecutionSet]
        fromMap m = fmap snd (Map.toAscList $ invertMap m)

        edgesMap :: Map Feature (Set Feature)
        edgesMap = Map.fromListWith Set.union (map (second Set.singleton) $ edges g)


addFeatureToExecutionSets :: Map Feature (Set Feature) -> Map Feature Int -> Feature -> Map Feature Int
addFeatureToExecutionSets g sets f = Map.insert f (findCompatibleExecutionSet g sets f) sets


invertMap :: (Ord k, Ord v) => Map k v -> Map v [k]
invertMap m = Map.fromListWith (++) $ flipAL $ Map.toList m


findCompatibleExecutionSet :: Map Feature (Set Feature) -> Map Feature Int -> Feature -> Int
findCompatibleExecutionSet g sets feat = case mapMaybe (`Map.lookup` sets) outgoing of
    [] -> 0
    x -> maximum x + 1
    where
        outgoing :: [Feature]
        outgoing = fromMaybe [] (Set.elems <$> Map.lookup feat g)
