module DependencySolver.Internal.Validation
    ( validateAllDeterminantFeaturesExist
    , validateDependentFeaturesAreIncludedInGroup
    ) where

import DependencySolver.Internal.Domain

import Data.List (intercalate)
import Data.Set (Set)
import qualified Data.Set as Set

validateAllDeterminantFeaturesExist :: [Group] -> Either [ErrorMessage] [Group]
validateAllDeterminantFeaturesExist gs =
    if Set.size diff > 0 then Left [errorMessage] else Right gs
    where
        feats        = Set.unions $                   map features gs
        determinants = Set.fromList $ map determinant $ Set.elems $ Set.unions $ map dependencies gs
        diff         = Set.difference determinants feats
        errorMessage = "Features(s) " ++ showFeatureSet diff ++ " are not in any group"


validateDependentFeaturesAreIncludedInGroup :: [Group] -> Either [ErrorMessage] [Group]
validateDependentFeaturesAreIncludedInGroup gs = case errors of
        [] -> Right gs
        _ -> Left errors
    where
        errors = concatMap validateInternalDependencies gs


validateInternalDependencies :: Group -> [ErrorMessage]
validateInternalDependencies g = [errorMessage | Set.size difference > 0]
    where
        feats        = features g
        dependents   = Set.fromList $ map dependent $ Set.elems $ dependencies g
        difference   = Set.difference dependents feats
        errorMessage = "Dependent feature(s) " ++ showFeatureSet difference ++ " are not in group " ++ show g


showFeatureSet :: Set Feature -> String
showFeatureSet s = intercalate ", " (map getName $ Set.toList s)
