module DependencySolver.Internal.Parser (parseGroups) where

import DependencySolver.Internal.Domain
    ( Feature(..)
    , dependsOn
    , Group(Group)
    , Dependency
    , ErrorMessage
    , excludedFeatureNameCharacters
    )

import qualified Data.Set as Set
import Text.ParserCombinators.Parsec
import Data.Text (pack, unpack, strip)

groups :: GenParser Char st [Group]
groups = endBy group (try $ many eol) <* eof
    where eol =     try (string "\n\r")
                <|> try (string "\r\n")
                <|> string "\n"
                <|> string "\r"


group :: GenParser Char st Group
group =
    do  f <- features
        d <- dependencies <|> return []
        return $ Group (Set.fromList f) (Set.fromList d)


features :: GenParser Char st [Feature]
features = between (string "(") (string ")") $ many feature


feature :: GenParser Char st Feature
feature =
    do  feat <- spaces *> many1 (noneOf excludedFeatureNameCharacters)
        _ <- optional (char ',')
        return $ Feature $ unpack . strip . pack $ feat


dependencies :: GenParser Char st [Dependency]
dependencies = between (char '[') (char ']') $ many dependency


dependency :: GenParser Char st Dependency
dependency =
    do  dep <- feature
        _ <- string "->"
        det <- feature
        _ <- optional (char ',')
        return $ dep `dependsOn` det


parseGroups :: String -> Either [ErrorMessage] [Group]
parseGroups input = case parse groups "" input of
    Left e -> Left [show e]
    Right output -> Right output

